export const serv1 = (): string => {
  return 'reviews';
};

export const serv2 = (id: string): string => {
  return `review ${id}`;
};
