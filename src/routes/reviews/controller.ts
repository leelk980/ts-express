import { Request, Response, NextFunction } from 'express';
import { catchAsync } from 'src/utils';
import { serv1, serv2 } from './service';

export const func1 = catchAsync(async (_: Request, res: Response, __: NextFunction) => {
  const data = serv1();

  res.send(`hihi ${data}`);
});

export const func2 = catchAsync(async (req: Request, res: Response, __: NextFunction) => {
  const data = serv2(req.params.id);

  res.send(`byebye ${data}`);
});
