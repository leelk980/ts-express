import { Router } from 'express';
import { func1, func2 } from './controller';

const router = Router();

router.route('/').get(func1).post();
router.route('/:id').get(func2).post();

export default router;
