// eslint-disable-next-line
import * as request from 'supertest';
import app from '../../../app';

const tests = (): void => {
  it('returns all reviews', async () => {
    await request(app).get('/api/reviews/').expect(200);
  });

  it('returns 400', async () => {
    await request(app).get('/api/reviews/12/12').expect(400);
  });
};

export default tests;
