import getAllReviewsTest from './get.all.reviews.spec';
import getOneReviewsTest from './get.one.review.spec';

const tests = (): void => {
  describe('[GET] /api/reviews', getAllReviewsTest);
  describe('[GET] /api/reviews/:id', getOneReviewsTest);
};

export default tests;
