import * as jwt from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';
import { AppError, catchAsync } from 'src/utils';
import { User } from 'src/models';
import { CreateUserDto, LoginUserDto } from './dto';

export const register = catchAsync(async (req: Request, res: Response, __: NextFunction) => {
  const userRepository = getRepository(User);

  const user = await userRepository.save(userRepository.create(req.body as CreateUserDto));

  user.password = undefined;
  res.status(200).json({
    status: 'success',
    user,
  });
});

export const login = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const userRepository = getRepository(User);

  const { email, password } = req.body as LoginUserDto;
  const user = await userRepository.findOne({ where: { email, password } });

  if (!user) return next(new AppError('Cannot find user!', 400));

  const token = jwt.sign({ id: user.id }, process.env.JWT_SECRET!, {
    expiresIn: process.env.JWT_EXPIRES_IN,
  });

  return res.status(200).json({
    status: 'success',
    token,
  });
});

export const logout = catchAsync(async (_: Request, res: Response, __: NextFunction) => {
  const token = jwt.sign({ logout: 'logout' }, 'logout', {
    expiresIn: '1s',
  });

  res.status(200).json({
    status: 'success',
    token,
  });
});
