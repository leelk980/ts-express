import { body, ValidationChain } from 'express-validator';

export type LoginUserDto = {
  email: string;
  password: string;
};

export const LoginUserValidation: ValidationChain[] = [
  body('email').trim().isEmail().isLength({ min: 2, max: 20 }),
  body('password').trim().isString().isLength({ min: 2, max: 10 }),
];
