import { body, ValidationChain } from 'express-validator';

export type CreateUserDto = {
  name: string;
  email: string;
  password: string;
};

export const CreateUserValidation: ValidationChain[] = [
  body('name').trim().not().isNumeric().isLength({ min: 2, max: 10 }),
  body('email').trim().isEmail().isLength({ min: 2, max: 20 }),
  body('password').trim().isString().isLength({ min: 2, max: 10 }),
];
