// eslint-disable-next-line
import * as request from 'supertest';
import app from '../../../app';

const tests = (): void => {
  it('returns single tour', async () => {
    await request(app).get('/api/tours/1').expect(200);
  });

  it('returns 400', async () => {
    await request(app).get('/api/tours/12/1').expect(400);
  });
};

export default tests;
