import getAllToursTest from './get.all.tours.spec';
import getOneTourTest from './get.one.tour.spec';

const tourRoutesTest = (): void => {
  describe('[GET] /api/tours', getAllToursTest);
  describe('[GET] /api/tours/:id', getOneTourTest);
};

export default tourRoutesTest;
