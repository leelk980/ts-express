// eslint-disable-next-line
import * as request from 'supertest';
import app from '../../../app';

const tests = (): void => {
  it('returns all tours', async () => {
    await request(app).get('/api/tours/').expect(200);
  });

  it('returns 400', async () => {
    await request(app).get('/api/tours/as/as').expect(400);
  });
};

export default tests;
