import { Router } from 'express';
import { authGuard, runValidator } from 'src/utils';
import { register, login, logout } from './controller';
import { CreateUserValidation, LoginUserValidation } from './dto';

const router = Router();

router.post('/signup', runValidator(CreateUserValidation), register);
router.post('/signin', runValidator(LoginUserValidation), login);
router.post('/signout', authGuard(), logout);

export default router;
