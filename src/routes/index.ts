import tours from './tours';
import reviews from './reviews';
import auth from './auth';

export { tours, reviews, auth };
