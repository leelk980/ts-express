import { body, ValidationChain } from 'express-validator';

export type UpdateTourDto = {
  title?: string;
  content?: string;
  location?: string;
};

export const UpdateTourValidation: ValidationChain[] = [
  body('title').trim().not().isNumeric().isLength({ min: 2, max: 10 }).optional(),
  body('content').trim().isString().isLength({ min: 2, max: 10 }).optional(),
  body('location').trim().isString().isLength({ min: 2, max: 10 }).optional(),
];
