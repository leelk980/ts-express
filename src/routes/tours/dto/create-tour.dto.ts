import { body, ValidationChain } from 'express-validator';

export type CreateTourDto = {
  title: string;
  content: string;
  location: string;
};

export const CreateTourValidation: ValidationChain[] = [
  body('title').trim().not().isNumeric().isLength({ min: 2, max: 10 }),
  body('content').trim().isString().isLength({ min: 2, max: 10 }),
  body('location').trim().isString().isLength({ min: 2, max: 10 }),
];
