import { Router } from 'express';
import { runValidator, rolesGuard } from 'src/utils';
import { CreateTourValidation, UpdateTourValidation } from './dto';
import {
  createOneTour,
  updateOneTourById,
  deleteOneTourById,
  getOneTourById,
  getManyTours,
} from './controller';

const router = Router();

// prettier-ignore
router.route('/')
  .get(getManyTours)
  .post(runValidator(CreateTourValidation),  createOneTour);

router
  .route('/:id')
  .get(getOneTourById)
  .put(rolesGuard({ allowTo: ['admin'] }), runValidator(UpdateTourValidation), updateOneTourById)
  .delete(deleteOneTourById);

export default router;
