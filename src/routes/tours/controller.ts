import { crudHandler } from 'src/utils';
import { Tour } from 'src/models';

export const createOneTour = crudHandler.createOne(Tour);
export const updateOneTourById = crudHandler.updateOne(Tour);
export const deleteOneTourById = crudHandler.deleteOne(Tour);
export const getOneTourById = crudHandler.getOne(Tour, { relations: ['users'] });
export const getManyTours = crudHandler.getMany(Tour, [{ relation: 'tour.users', alias: 'user' }]);
