/* eslint-disable */
import * as dotenv from 'dotenv';
import { createConnection } from 'typeorm';

import app from './app';

dotenv.config({ path: './.env' });

const { NODE_ENV, PORT_DEV, PORT_PROD } = process.env;

createConnection({
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: process.env.DB_USER,
  password: process.env.DB_PASSWD,
  database: process.env.DB_NAME,
  schema: process.env.DB_SCHEMA,
  entities: [__dirname + '/models/*.ts'],
  synchronize: true,
})
  .then(() => console.log('Database connected!'))
  .catch((error) => console.log(`Database unconnected! ${error}`));

const port = NODE_ENV === 'development' ? PORT_DEV : PORT_PROD;

app.listen(port, () => console.log(`Running on ${port}!`));
