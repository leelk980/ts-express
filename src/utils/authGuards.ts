import { Request, Response, NextFunction, RequestHandler } from 'express';
import * as jwt from 'jsonwebtoken';
import { getRepository } from 'typeorm';
import { User } from 'src/models';
import AppError from './AppError';
import catchAsync from './catchAsync';

type authGuardParam = {
  excepts?: string[];
};

type rolesGuardParam = {
  allowTo: string[];
};

type decodedToken = {
  id: number;
  iat: string;
  exp: string;
};

const authGuard = ({ excepts }: authGuardParam = {}): RequestHandler =>
  catchAsync(async (req: Request, _: Response, next: NextFunction) => {
    if (excepts?.includes(req.method)) return next();

    const userRepository = getRepository(User);
    const token = req.headers?.authorization?.split(' ')[1];

    if (!token) {
      return next(new AppError('You are not logged in! Please log in to get access.', 400));
    }

    const decoded = jwt.verify(token, process.env.JWT_SECRET!) as decodedToken;

    const user = await userRepository.findOne(decoded.id);

    if (!user)
      return next(new AppError('You are not logged in! Please log in to get access.', 400));

    // 4) Check if user changed password after the token was issued

    req.user = user;
    return next();
  });

export const rolesGuard = ({ allowTo }: rolesGuardParam) => (
  req: Request,
  __: Response,
  next: NextFunction,
): void => {
  if (!allowTo.includes(req.user.role))
    return next(new AppError('You do not have permission to perform this action', 400));

  return next();
};

export const setUserId = (req: Request, __: Response, next: NextFunction): void => {
  req.body.userId = req.user.id;
  next();
};

export default authGuard;
