import { Request, Response, NextFunction, RequestHandler } from 'express';
import { FindOneOptions, getRepository } from 'typeorm';
import AppError from './AppError';
import catchAsync from './catchAsync';

class A {
  [props: string]: any;
}

type leftJoinAndSelectType = {
  relation: string;
  alias: string;
  condition?: string;
  param?: {
    [str: string]: any;
  };
};

export const createOne = <T extends typeof A>(Model: T): RequestHandler =>
  catchAsync(async (req: Request, res: Response, next: NextFunction) => {
    const modelRepository = getRepository(Model);

    let record = new Model();
    record = req.body;
    record = await modelRepository.save(record);
    if (!record) return next(new AppError('Cannot create record!', 400));

    return res.status(200).json({
      status: 'success',
      [Model.name.toLocaleLowerCase()]: record,
    });
  });

export const updateOne = <T extends typeof A>(Model: T): RequestHandler =>
  catchAsync(async (req: Request, res: Response, next: NextFunction) => {
    const modelRepository = getRepository(Model);

    let record = await modelRepository.findOne(req.params.id);
    if (!record) return next(new AppError('Cannot update record!', 400));
    if (req.user.role !== 'admin' && record.userId && record.userId !== req.user.id)
      return next(new AppError('Cannot update record!', 400));

    Object.keys(req.body).forEach((key) => {
      record![key] = req.body[key];
    });

    record = await modelRepository.save(record);

    return res.status(200).json({
      status: 'success',
      [Model.name.toLocaleLowerCase()]: record,
    });
  });

export const deleteOne = <T extends typeof A>(Model: T): RequestHandler =>
  catchAsync(async (req: Request, res: Response, next: NextFunction) => {
    const modelRepository = getRepository(Model);

    const record = await modelRepository.findOne(req.params.id);
    if (!record) return next(new AppError('Cannot delete record!', 400));
    if (req.user.role !== 'admin' && record.userId && record.userId !== req.user.id)
      return next(new AppError('Cannot update record!', 400));

    await modelRepository.remove(record);
    return res.status(200).json({
      status: 'success',
    });
  });

export const getOne = <T extends typeof A>(Model: T, options?: FindOneOptions): RequestHandler =>
  catchAsync(async (req: Request, res: Response, next: NextFunction) => {
    const modelRepository = getRepository(Model);

    const record = await modelRepository.findOne(req.params.id, options);
    if (!record) return next(new AppError('Cannot find record by this ID!', 400));

    return res.status(200).json({
      status: 'success',
      [Model.name.toLocaleLowerCase()]: record,
    });
  });

export const getMany = <T extends typeof A>(
  Model: T,
  leftJoinAndSelects: leftJoinAndSelectType[],
): RequestHandler =>
  catchAsync(async (req: Request, res: Response, __: NextFunction) => {
    const modelRepository = getRepository(Model);

    const { limit, page, order, search } = req.query;
    const take = limit ? +limit : 10;
    const skip = page ? (+page - 1) * take : 0;

    const recordsQuery = modelRepository.createQueryBuilder(Model.name.toLocaleLowerCase());

    if (leftJoinAndSelects) {
      leftJoinAndSelects.forEach((join) => {
        recordsQuery.leftJoinAndSelect(join.relation, join.alias, join.condition, join.param);
      });
    }

    if (search) {
      const { param, operator, term } = JSON.parse(search as string);
      recordsQuery.where(`${Model.name.toLocaleLowerCase()}.${param} ${operator} :searchTerm`, {
        searchTerm: operator === 'ilike' ? `%${term}%` : term,
      });
    }

    if (order) recordsQuery.orderBy(`${Model.name.toLocaleLowerCase()}.${order}`, 'DESC');
    recordsQuery.skip(skip).take(take);

    const records = await recordsQuery.getMany();

    return res.status(200).json({
      status: 'success',
      [Model.name.toLocaleLowerCase()]: records,
    });
  });

// const user = await createQueryBuilder('user')
//   .leftJoinAndSelect('user.photos', 'photo', 'photo.isRemoved = :isRemoved', { isRemoved: false })
//   .leftJoinAndSelect('user.photos', 'photo', 'photo.isRemoved = :isRemoved', { isRemoved: false })
//   .leftJoinAndSelect('user.photos', 'photo', 'photo.isRemoved = :isRemoved', { isRemoved: false })
//   .where('user.name = :name', { name: 'Timber' })
//   .orderBy("user.name")
//   .skip(5)
//   .take(10)
//   .getMany()
