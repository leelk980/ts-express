import { Request, Response, NextFunction, RequestHandler } from 'express';

type asyncRequestHandler = (
  req: Request,
  res: Response,
  next: NextFunction,
) => Promise<Response | void>;

const catchAsync = (fn: asyncRequestHandler): RequestHandler => {
  return (req, res, next) => {
    fn(req, res, next).catch(next);
  };
};

export default catchAsync;
