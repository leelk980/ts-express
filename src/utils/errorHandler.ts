import { Request, Response, NextFunction } from 'express';
import AppError from './AppError';
import logger from './logger';

const sendErrorAtProd = (err: AppError, _: Request, res: Response) => {
  if (err.isOperational) {
    return res.status(err.statusCode).json({
      status: err.status,
      message: err.message,
    });
  }

  return res.status(500).json({
    status: 'error',
    message: 'Server Error, please try again',
  });
};

const sendErrorAtDev = (err: AppError, _: Request, res: Response) =>
  res.status(err.statusCode).json({
    status: err.status,
    error: err,
    message: err.message,
    stack: err.stack,
  });

const errorHandler = (err: Error, req: Request, res: Response, _: NextFunction): void => {
  // console.log(err.stack);
  const error = err as AppError;

  error.statusCode = error.statusCode ?? 500;
  error.status = error.status ?? 'error';
  error.isOperational = error.isOperational ?? false;

  logger.error(`[${req.method}] ${req.originalUrl}
    statusCode: ${error.statusCode}
    status: ${error.status}
    isOperational: ${error.isOperational}
    message: ${error.message}
  `);

  if (process.env.NODE_ENV === 'production') {
    sendErrorAtProd(error, req, res);
  } else {
    sendErrorAtDev(error, req, res);
  }
};

export default errorHandler;
