import { ValidationChain, validationResult } from 'express-validator';
import { NextFunction, Request, RequestHandler, Response } from 'express';
import AppError from './AppError';
import catchAsync from './catchAsync';

const checkIfExtraFields = (validationRules: ValidationChain[], req: Request) => {
  const allowedFields = validationRules.map((rule) => rule.builder.build().fields.join()).sort();

  const requestInput = { ...req.query, ...req.body }; // exclude params...
  const requestFields = Object.keys(requestInput).sort();

  const extraFields = requestFields.filter((field) => !allowedFields.includes(field));

  return extraFields.length === 0 ? false : extraFields;
};

const validator = (
  validationRules: ValidationChain[],
  allowExtraFields?: boolean,
): RequestHandler =>
  catchAsync(
    async (req: Request, _: Response, next: NextFunction): Promise<void> => {
      if (!allowExtraFields) {
        const extraFields = checkIfExtraFields(validationRules, req);

        if (extraFields) {
          return next(new AppError(`[${extraFields.join(', ')}] is(are) not allowed.`, 400));
        }
      }

      await Promise.all(validationRules.map((rule) => rule.run(req)));
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        let errorMessages = errors
          .array()
          .map(
            ({ location, param, value }) => `[req.${location}.${param}] - ${value} is not valid!`,
          );

        errorMessages = errorMessages.filter((msg, index, self) => index === self.indexOf(msg));

        return next(new AppError(errorMessages.join(','), 400));
      }

      return next();
    },
  );

export default validator;
