/* eslint-disable */
import { Request, Response, NextFunction } from 'express';
import * as winston from 'winston';
import * as WinstonDaily from 'winston-daily-rotate-file';

const { combine, timestamp, printf, colorize } = winston.format;

const logFormat = printf(
  ({ timestamp, level, message }) => `${timestamp} [${level.toUpperCase()}]: ${message}`,
);

const colorLogFormat = printf(({ timestamp, level, message }) =>
  colorize().colorize(level, `${timestamp} [${level.toUpperCase()}]: ${message}`),
);

let logger: winston.Logger;

if (process.env.NODE_ENV === 'production') {
  const prodLogger = winston.createLogger({
    format: combine(
      timestamp({
        format: 'YYYY-MM-DD HH:mm:ss',
      }),
      logFormat,
    ),
    defaultMeta: { service: 'test' },
    transports: [
      new WinstonDaily({
        level: 'error',
        filename: `%DATE%.error.log`,
        dirname: './logs/error',
        datePattern: 'YYYY-MM-DD',
        zippedArchive: true,
        maxSize: '20m',
        maxFiles: '14d',
      }),
      new WinstonDaily({
        level: 'info',
        filename: '%DATE%.info.log',
        dirname: './logs',
        datePattern: 'YYYY-MM-DD',
        zippedArchive: true,
        maxSize: '20m',
        maxFiles: '14d',
      }),
    ],
    exceptionHandlers: [
      new WinstonDaily({
        level: 'exception',
        filename: `%DATE%.exception.log`,
        dirname: './logs/exception',
        datePattern: 'YYYY-MM-DD',
        zippedArchive: true,
        maxSize: '20m',
        maxFiles: '14d',
      }),
    ],
  });

  logger = prodLogger;
} else {
  const devLogger = winston.createLogger({
    format: combine(
      timestamp({
        format: 'YYYY-MM-DD HH:mm:ss',
      }),
      colorLogFormat,
    ),
    defaultMeta: { service: 'test' },
    transports: [new winston.transports.Console()],
    exceptionHandlers: [new winston.transports.Console()],
  });

  logger = devLogger;
}

export default logger;
export const loggingMiddleware = (req: Request, _: Response, next: NextFunction) => {
  logger.info(`[${req.method}] ${req.originalUrl}`);
  next();
};
