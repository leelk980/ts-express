import * as express from 'express';
import * as cors from 'cors';
import * as compression from 'compression';
import * as helmet from 'helmet';
import * as hpp from 'hpp';
import * as rateLimit from 'express-rate-limit';
import * as swaggerUi from 'swagger-ui-express';
import * as swaggerJsdoc from 'swagger-jsdoc';
import 'reflect-metadata';

import { AppError, authGuard, errorHandler, loggingMiddleware } from './utils';
import { corsOrigin, hppWhiteList, rateLimiterMessage, swaggerOptions } from './config';
import * as Routers from './routes';

const app = express();

app.enable('trust proxy');

// security
app.use(cors({ origin: corsOrigin, credentials: true }));
app.use(helmet());
app.use(rateLimit({ max: 100, windowMs: 60 * 1000, message: rateLimiterMessage }));

// others
app.use(express.json());
app.use(hpp({ whitelist: hppWhiteList }));
app.use(compression());
app.use(loggingMiddleware);

// routes
app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerJsdoc(swaggerOptions)));
app.use('/api/auth', Routers.auth);

// protected routes
app.use(authGuard({ excepts: ['GET'] }));
app.use('/api/tours', Routers.tours);
app.use('/api/reviews', Routers.reviews);

app.all('*', (req, res, next) =>
  next(new AppError(`Can't find [${req.method}] ${req.originalUrl} on this server!`, 400)),
);

app.use(errorHandler);

export default app;
