/**
 * @swagger
 * tags:
 *   name: Auth
 *   description: The auth managing API. Response = Success(200) | Fail(400) | Error(500)
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     User:
 *       type: object
 *       required:
 *         - email
 *         - password
 *       properties:
 *         id:
 *           type: number
 *         email:
 *           type: string
 *         name:
 *           type: string
 *         tours:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/Tour'
 *       example:
 *         email: abc@abc.com
 *         name: Alexander
 *         password: 123123
 */
