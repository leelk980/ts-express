/**
 * @swagger
 * /tours:
 *   post:
 *     summary: Create a new tour
 *     tags: [Tours]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Tour'
 */

/**
 * @swagger
 * /tours/{id}:
 *   put:
 *     summary: Update the tour by the id
 *     tags: [Tours]
 *     parameters:
 *       - name: id
 *         in: path
 *         type: number
 *         required: true
 *     requestBody:
 *       required: false
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Tour'
 */

/**
 * @swagger
 * /tours/{id}:
 *   delete:
 *     summary: Remove the tour by id
 *     tags: [Tours]
 *     parameters:
 *       - name: id
 *         in: path
 *         type: number
 *         required: true
 */

/**
 * @swagger
 * /tours:
 *   get:
 *     summary: Returns the list of all the tours
 *     tags: [Tours]
 */

/**
 * @swagger
 * /tours/{id}:
 *   get:
 *     summary: Get the tour by id
 *     tags: [Tours]
 *     parameters:
 *       - name: id
 *         in: path
 *         type: number
 *         required: true
 */

/**
 *     responses:
 *       200:
 *         description: success
 *       400:
 *         description: fail
 *       500:
 *         description: error
 */
