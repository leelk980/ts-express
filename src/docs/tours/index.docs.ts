/**
 * @swagger
 * tags:
 *   name: Tours
 *   description: The auth managing API. Response = Success(200) | Fail(400) | Error(500)
 *
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     Tour:
 *       type: object
 *       required:
 *         - title
 *         - content
 *       properties:
 *         id:
 *           type: number
 *         title:
 *           type: string
 *         content:
 *           type: string
 *         user:
 *           $ref: '#/components/schemas/User'
 *       example:
 *         title: The New Turing Omnibus
 *         content: Alexander K. Dewdney
 */
