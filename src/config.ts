import * as swaggerJSDoc from 'swagger-jsdoc';

export const corsOrigin = ['http://localhost:3000', 'http://localhost:3001'];

export const rateLimiterMessage = 'Too many requests from this IP, please try again in an hour!';

export const hppWhiteList = ['params1', 'params2', 'params3'];

export const swaggerOptions: swaggerJSDoc.Options = {
  definition: {
    components: {},
    openapi: '3.0.0',
    info: {
      title: 'Express API with Swagger',
      version: '1.0.0',
      description:
        'This is a simple CRUD API application made with Express and documented with Swagger.',
    },
    produces: ['application/json'],
    servers: [
      {
        url: 'http://localhost:3000/api/',
      },
    ],
  },
  apis: ['src/docs/**/*.ts'],
};
