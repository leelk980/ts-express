/* eslint-disable import/no-cycle */
import { Entity, Column, ManyToOne, JoinColumn } from 'typeorm';
import { Base, User, Tour } from './index';

@Entity()
export default class Reservation extends Base {
  @Column()
  price!: number;

  @Column({ select: false })
  userId?: number;

  @Column({ select: false })
  tourId?: number;

  @ManyToOne(() => User, (user) => user.reservations, { primary: true })
  @JoinColumn()
  user?: User;

  @ManyToOne(() => Tour, (tour) => tour.reservations, { primary: true })
  @JoinColumn()
  tour?: Tour;
}
