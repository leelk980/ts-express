/* eslint-disable import/no-cycle */
import { Entity, Column, OneToOne, JoinColumn } from 'typeorm';
import { Base, User } from './index';

@Entity()
export default class Profile extends Base {
  @Column()
  age!: number;

  @Column({ select: false })
  userId?: number;

  @OneToOne(() => User, (user) => user.profile)
  @JoinColumn()
  user?: User;
}
