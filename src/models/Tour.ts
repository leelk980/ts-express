/* eslint-disable import/no-cycle */
import { Entity, Column, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { Base, User, Reservation, Review } from './index';

@Entity()
export default class Tour extends Base {
  @Column()
  title!: string;

  @Column()
  content!: string;

  @Column()
  location!: string;

  @OneToMany(() => Review, (review) => review.tour)
  reviews?: Review[];

  @OneToMany(() => Reservation, (reservation) => reservation.tour)
  reservations?: Reservation[];

  @ManyToMany(() => User, (user) => user.tours)
  @JoinTable({ name: 'reservation' })
  users?: User[];
}
