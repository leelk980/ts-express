/* eslint-disable import/no-cycle */
import { Entity, Column, OneToMany, ManyToMany, OneToOne } from 'typeorm';
import { Base, Tour, Reservation, Review, Profile } from './index';

@Entity()
export default class User extends Base {
  @Column()
  name!: string;

  @Column()
  email!: string;

  @Column({ select: false })
  password?: string;

  @OneToOne(() => Profile, (profile) => profile.user)
  profile?: Profile;

  @OneToMany(() => Review, (review) => review.user)
  reviews?: Review[];

  @OneToMany(() => Reservation, (reservation) => reservation.user)
  reservations?: Reservation[];

  @ManyToMany(() => Tour, (tour) => tour.users)
  tours?: Tour[];
}
