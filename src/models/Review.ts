/* eslint-disable import/no-cycle */
import { Entity, Column, ManyToOne, JoinColumn } from 'typeorm';
import { Base, User, Tour } from './index';

@Entity()
export default class Review extends Base {
  @Column()
  title!: string;

  @Column()
  content!: string;

  @Column({ select: false })
  userId?: number;

  @Column({ select: false })
  tourId?: number;

  @ManyToOne(() => User, (user) => user.reviews)
  @JoinColumn()
  user?: User;

  @ManyToOne(() => Tour, (tour) => tour.reviews)
  @JoinColumn()
  tour?: Tour;
}
