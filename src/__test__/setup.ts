// eslint-disable-next-line
import * as request from 'supertest';
import app from '../app';

beforeAll(async () => {
  // connect DB, import Env ...
  console.log('TEST START!');
});

beforeEach(async () => {
  // before run test, clean up db
  console.log('EACH');
});

afterAll(async () => {
  // excecute once at end. disconnect to mongodb(memory db)
  console.log('Finish!');
});

global.getCookie = async () => {
  const email = 'test@test.com';
  const password = 'password';

  const res = await request(app).post('api/users/signup').send({ email, password }).expect(201);

  return res.get('Set-Cookie');
};
